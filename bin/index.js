#!/usr/bin/env node
const ruuvi = require('node-ruuvitag');
const db = require('better-sqlite3')('../environment.db', {
    verbose: console.log,
});

const createTableStatement = (table) =>
    db.prepare(
        `CREATE TABLE IF NOT EXISTS "${table}" (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            data TEXT,
            dateTimeUtc TEXT DEFAULT CURRENT_TIMESTAMP
        )`
    );
const insertDataStatement = (table) =>
    db.prepare(`INSERT INTO "${table}" (data) VALUES (?)`);

function onFindTag(tag) {
    console.log('Getting data for tag ' + tag.id);

    return new Promise((resolve, reject) => {
        tag.on('updated', function onTagUpdated(data) {
            console.log(
                'Got data from RuuviTag ' +
                    tag.id +
                    ':\n' +
                    JSON.stringify(data, null, '\t')
            );

            createTableStatement(tag.id).run();
            insertDataStatement(tag.id).run(JSON.stringify(data));
            resolve();
        });
    });
}

Promise.resolve()
    .then(() => {
        ruuvi.on('warning', (message) => {
            throw new Error(message);
        });

        return ruuvi.findTags();
    })
    .then((tags) => tags.reduce((acc, tag) => acc.then(() => onFindTag(tag)), Promise.resolve()))
    .then(() => {
        db.close();
        process.exit(0);
    })
    .catch((err) => {
        db.close();
        console.error(err);
        process.exit(1);
    });
